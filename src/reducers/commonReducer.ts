import CommonActionTypes, { AUTH } from "../actions/CommonActions/commonActionTypes";

interface ICommonState {
    isLoggedIn: boolean;
}

const defaultCommonState : ICommonState ={
    isLoggedIn: false,
}

const commonReducer = (state: ICommonState = defaultCommonState, action: CommonActionTypes) : ICommonState =>{
    switch(action.type){
        case AUTH:
            return {isLoggedIn: action.isLoggedIn};
        default:
            return state;
    }
}

export default commonReducer;