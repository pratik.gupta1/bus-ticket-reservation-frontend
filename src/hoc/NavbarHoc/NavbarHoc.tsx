import React, { Component } from "react";
import { connect } from "react-redux";
import { NavLink, withRouter } from "react-router-dom";
import commonActions from "../../actions/CommonActions/commonActions";
import { ICombinedState } from "../../store";
import { BoundThunk } from "../../utils";

export function withNavbarHoc<T>(WrappedComponent: React.ComponentType<T>) {
  const mapStateToProps = (state: ICombinedState) => ({
    isLoggedIn: state.common.isLoggedIn,
  });
  
  const mapDispatchToProps = commonActions;
  type IDispatchProps = { [P in keyof typeof mapDispatchToProps]: BoundThunk<typeof mapDispatchToProps[P]> };
  type IProps = IDispatchProps & ReturnType<typeof mapStateToProps> & T;
  

  class NavbarHoc extends Component<any> {
    render() {
      return (
        <>
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <NavLink to="/" className="nav-link" exact>
              Home
            </NavLink>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav m-auto">
                <li className="nav-item">
                  <NavLink to="/" className="nav-link" exact>
                    New Booking
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to="/buses" className="nav-link" exact>
                    Buses
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to="/routes" className="nav-link" exact>
                    Routes
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink to="/manage-bookings" className="nav-link" exact>
                    Mange Bookings
                  </NavLink>
                </li>
              </ul>
              {this.props.isLoggedIn ? <form className="form-inline my-2 my-lg-0 me-3">
                <button className="btn btn-outline-success my-2 my-sm-0" onClick={(e)=>{this.props.setAuth(false); this.props.history.push('/');}}>
                  Logout
                </button>
              </form>
              :
              <form className="form-inline my-2 my-lg-0 px-3 me-3">
                <button className="btn btn-outline-success my-2 my-sm-0" onClick={(e)=>{this.props.history.push('/signin');}}>
                  Signin
                </button>
              </form>
              }
            </div>
          </nav>
          <div className="w-100">
            <WrappedComponent {...(this.props as T)} />
          </div>
        </>
      );
    }
  };
  return withRouter(connect(mapStateToProps, mapDispatchToProps)(NavbarHoc));
}

export default withNavbarHoc;
