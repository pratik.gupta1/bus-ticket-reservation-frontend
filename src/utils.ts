import { Dispatch } from "react";
import ActionTypes from "./actions/actionTypes";
import { IBus, IRoute } from "./models/models";
import { ThunkAction } from "redux-thunk";
import axios from "./axiosInstance";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const toMonthStr = (monthIndex: number) => {
  return monthNames[monthIndex];
};

export function formatDate(date: string | Date): string {
  let d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [year, month, day].join("-");
}

export function showError(msg: string, dispatch: Dispatch<ActionTypes>) {
  // TODO : complete this
  console.log("please complete showError function");
}

export const EmptyBus: IBus = {
  vehicleMake: "",
  vehicleModel: "",
  vehicleNumber: "",
  busType: "",
  images: [],
  numOfSeats: 0,
};

export const EmptyRoute: IRoute = {
  from: "",
  to: "",
  travelDuration: 0,
  departureHour: 0,
  departureMinute: 0,
  days: [],
  price: 0,
  bus: "",
};

export const isSignedIn = async (): Promise<boolean> => {
  if (window.localStorage.getItem("x-access-token") === null) {
    return false;
  }
  axios.defaults.headers["x-access-token"] = window.localStorage.getItem("x-access-token");
  try {
    await axios.get("/admin/verifyToken");
    return true;
  } catch (error) {
    return false;
  }
};

export type BoundThunk<T extends (...args: any[]) => ThunkAction<any, any, any, any>> = (
  ...args: Parameters<T>
) => ReturnType<ReturnType<T>>;

export function capitalizeFirstLetter(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}
