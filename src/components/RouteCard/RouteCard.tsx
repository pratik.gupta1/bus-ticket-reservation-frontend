import { IBus, IRoute } from "../../models/models";
import { capitalizeFirstLetter } from "../../utils";
import EditRoute from "../EditRoute/EditRoute";
import styles from "./RouteCard.module.css";
interface IRouteCardProps extends IRoute {
  buses : IBus[];
  editRoute: (routeData: IRoute, routeId: IRoute["_id"]) => any;
  postRoute: (routeData: IRoute) => any;
  deleteRoute: (routeId: IRoute["_id"]) => any;
}

const weekDaysLabels = ["", "S", "M", "T", "W", "T", "F", "S"];

const RouteCard: React.FunctionComponent<IRouteCardProps> = (props) => {
  const departureDate = new Date();
  departureDate.setHours(props.departureHour);
  departureDate.setMinutes(props.departureMinute);
  const arrivalDate = new Date(departureDate.getTime() + props.travelDuration);
  const arrivalHour = arrivalDate.getHours();
  const arrivalMinute = arrivalDate.getMinutes();
  let days = new Array(8).fill(false);
      for (let weekNum of props.days) {
        days[weekNum] = true;
      }
  return (
      <>
    <div className="container card mb-3 p-3">
      <div className="row">
        <div className="col col-sm-12 col-md-6">
          <div className={`${styles.label}`}>From</div>
          <div className={`${styles.value}`}>{capitalizeFirstLetter(props.from)}</div>
        </div>

        <div className="col col-sm-12 col-md-6">
          <div className={`${styles.label} ml-auto`}>To</div>
          <div className={`${styles.value} ml-auto`}>{capitalizeFirstLetter(props.to)}</div>
        </div>
      </div>

      <div className="row">
        <div className="col col-sm-12 col-md-6">
          <div className={`${styles.label}`}>Departure Time</div>
          <div>
            <span className={`${styles.timeBold}`}>
              {props.departureHour.toString().padStart(2,"0")}:{props.departureMinute.toString().padStart(2,"0")}
            </span>
            <span className={`${styles.timeLight} p-2`}>{"Day 1"}</span>
          </div>
        </div>
        <div className="col col-sm-12 col-md-6">
          <div className={`${styles.label}`}>Arrival Time</div>
          <div>
            <span className={`${styles.timeBold}`}>
              {arrivalHour.toString().padStart(2,"0")}:{arrivalMinute.toString().padStart(2,"0")}
            </span>
            <span className={`${styles.timeLight} p-2`}>
              {`Day ${1 + (arrivalDate.getDate() - departureDate.getDate())}`}
            </span>
          </div>
        </div>
      </div>
      <div className={`${styles.weekDaysSelector}`}>
                {Array.from({ length: 7 })
                  .map((val, i) => i + 1)
                  .map((weekNum, i) => (
                    <span key={weekNum}
                      className={`${styles.weekDayLabel} ${days[weekNum] ? styles.weekDaySelected : ""}`}
                    >
                      {weekDaysLabels[weekNum]}
                    </span>
                  ))}
              </div>
              <div className="row">
                <div className="col">
                <div className="row justify-content-end">
                    <div className={`${styles.label}`}>BusId</div>
                  </div>
                  <div className="row justify-content-end">
                  <div className={`${styles.value} ml-auto`}>{props.bus}</div>
                  </div>
                </div>
                <div className="col">
                  <div className="row justify-content-end">
                    <div className={`${styles.label}`}>Price</div>
                  </div>
                  <div className="row justify-content-end">
                    <h3><span className="pe-2">₹</span>{props.price}</h3>
                  </div>
                </div>
              </div>
      <div className="row">
        <div className="col-auto p-2">
          <button className="btn btn-primary " data-bs-toggle="modal" data-bs-target={`#modal${props._id}`}>
            Edit
          </button>
        </div>
        <div className="col-auto p-2">
          <button className="btn btn-danger col-auto" onClick={(e) => props.deleteRoute(props._id)}>
            Delete
          </button>
        </div>
      </div>
    </div>
    <div
        className="modal fade"
        id={`modal${props._id}`}
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id={`modal${props._id}_title`}>
                Edit Route
              </h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                {/* <span aria-hidden="true">&times;</span> */}
              </button>
            </div>
            <div className="modal-body"><EditRoute {...props} isNew={false} postRoute={props.postRoute} editRoute={props.editRoute}/></div>
          </div>
        </div>
      </div>
    </>
  );
};

export default RouteCard;
