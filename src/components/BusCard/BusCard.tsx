import React, { useEffect, useState } from "react";
import { deleteBus } from "../../actions/BusActions/busActions";
import { IBus } from "../../models/models";
import EditBus from "../EditBus/EditBus";
import styles from "./BusCard.module.css";

interface IBusCardProps extends IBus {
  editBus: (busData: IBus, busId: IBus["_id"]) => any;
  deleteBus: (busId: IBus["_id"]) => any;
  postBus: (newBus: IBus) => any;
}

const BusCard: React.FunctionComponent<IBusCardProps> = (props) => {
  return (
    <>
      <div className="container card mb-3 p-3">
        <div className="row">
          <div className="col col-sm-6">
            <div className={`${styles.label}`}>Vehicle number</div>
            <div className={`${styles.value}`}>{props.vehicleNumber}</div>
          </div>
          <div className="col col-sm-6">
            <div className={`${styles.label}`}>Vehicle Id</div>
            <div className={`${styles.value}`}>{props._id}</div>
          </div>
        </div>
        <div className="row">
          <div className="col col-sm-12 col-md-6">
            <div className={`${styles.label}`}>Make</div>
            <div className={`${styles.value}`}>{props.vehicleMake}</div>
          </div>

          <div className="col col-sm-12 col-md-6">
            <div className={`${styles.label}`}>Vehicle model</div>
            <div className={`${styles.value}`}>{props.vehicleModel}</div>
          </div>
        </div>
        <div className="row">
          <div className="col col-sm-12 col-md-6">
            <div className={`${styles.label}`}>Bus type</div>
            <div className={`${styles.value}`}>{props.busType}</div>
          </div>

          <div className="col col-sm-12 col-md-6">
            <div className={`${styles.label}`}>Seats</div>
            <div className={`${styles.value}`}>{props.numOfSeats}</div>
          </div>
        </div>
        <div className="row">
          <div className="col-auto p-2">
            <button className="btn btn-primary ml-auto" data-bs-toggle="modal" data-bs-target={`#modal${props._id}`}>
              Edit
            </button>
          </div>
          <div className="col-auto p-2">
            <button className="btn btn-danger" onClick={(e) => props.deleteBus(props._id)}>
              Delete
            </button>
          </div>
        </div>
      </div>
      <div
        className="modal fade"
        id={`modal${props._id}`}
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-lg modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id={`modal${props._id}_title`}>
                Edit Bus
              </h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                {/* <span aria-hidden="true">&times;</span> */}
              </button>
            </div>
            <div className="modal-body">
              <EditBus {...props} isNew={false} postBus={props.postBus} editBus={props.editBus} />
            </div>
            {/* <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div> */}
          </div>
        </div>
      </div>
    </>
  );
};

export default BusCard;
