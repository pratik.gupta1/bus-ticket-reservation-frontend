import React from "react";
import { IReservationDetails } from "../../models/models";
import styles from "./ReservationDetails.module.css";

interface IReservationDetailsProps {
  cancelReservation: (reservationId: string) => Promise<void>;
  reservationDetails: IReservationDetails;
  selectedSeat: number | null;
}

const ReservationDetails: React.FunctionComponent<IReservationDetailsProps> = (props) => {
  return (
    <React.Fragment>
      {props.selectedSeat === null && <div className="text-center">No seat selected</div>}
      {props.selectedSeat && !props.reservationDetails[props.selectedSeat] && (
        <>
          <div className="w-100 d-flex justify-content-center">
            <h5>Seat details</h5>
          </div>
          <div className="text-center mb-3">Unreserved seat</div>
        </>
      )}
      {props.selectedSeat && props.reservationDetails[props.selectedSeat] && (
        <>
          <div className="w-100 d-flex justify-content-center">
            <h5>Seat details</h5>
          </div>
          <div className="conatiner w-100 p-3 mb-3">
            <div className="w-100 d-flex">
              <div className="col">
                <div className={`${styles.label}`}>Name</div>
                <div className={`${styles.value}`}>
                  {props.reservationDetails[props.selectedSeat].personDetails?.name}
                </div>
              </div>
            </div>
            <div className="w-100 d-flex">
              <div className="col">
                <div className={`${styles.label}`}>Email</div>
                <div className={`${styles.value}`}>
                  {props.reservationDetails[props.selectedSeat].personDetails?.email}
                </div>
              </div>
              <div className="col">
                <div className={`${styles.label}`}>Mobile</div>
                <div className={`${styles.value}`}>
                  {props.reservationDetails[props.selectedSeat].personDetails?.mobile}
                </div>
              </div>
            </div>
            <div className="w-100 d-flex">
              <div className="col col-sm-6">
                <div className={`${styles.label}`}>Age</div>
                <div className={`${styles.value}`}>
                  {props.reservationDetails[props.selectedSeat].personDetails?.age || "_"}
                </div>
              </div>
              <div className="col col-sm-6">
                <div className={`${styles.label}`}>Gender</div>
                <div className={`${styles.value}`}>
                  {props.reservationDetails[props.selectedSeat].personDetails?.gender || "_"}
                </div>
              </div>
            </div>
            <div className="w-100 d-flex justify-content-center">
              <button
                className="btn btn-danger"
                onClick={(e) => props.cancelReservation(props.reservationDetails[props.selectedSeat as number]._id)}
              >
                Cancel
              </button>
            </div>
          </div>
        </>
      )}
    </React.Fragment>
  );
};

export default ReservationDetails;
