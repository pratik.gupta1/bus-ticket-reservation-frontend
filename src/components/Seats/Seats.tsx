import React, { useState } from "react";
import { IPersonDetails, IReservation, IReservedSeats, IRoute } from "../../models/models";
import styles from "./Seats.module.css";

interface Iprops {
  numOfSeats: number;
  reservedSeats: IReservedSeats;
  selectedSeat: number | null;
  setSelectedSeat: (seatNumber: number) => void;
  isAdmin: boolean;
}

const Seats: React.FunctionComponent<Iprops> = (props) => {
  const ROWS_COUNT = 4;
  const perRowSeats = Math.floor(props.numOfSeats / ROWS_COUNT);
  let remSeats = props.numOfSeats % ROWS_COUNT;
  let rows: JSX.Element[][] = [];
  for (let i = 1; i <= ROWS_COUNT; ++i) {
    let curRow: JSX.Element[] = [];
    for (let j = 0; j < perRowSeats; ++j) {
      curRow.push(
        <div
          className={`${styles.seatWrapper} ${props.reservedSeats[j * ROWS_COUNT + i] ? styles.reserved : ""}
          ${!props.isAdmin && props.reservedSeats[j * ROWS_COUNT + i] ? styles.cursorNotAllowed : ""}
          ${props.selectedSeat === j * ROWS_COUNT + i ? styles.selected : ""}
          mx-2`}
          key={j * ROWS_COUNT + i}
          onClick={
            !props.isAdmin && props.reservedSeats[j * ROWS_COUNT + i]
              ? () => {}
              : (e) => props.setSelectedSeat(j * ROWS_COUNT + i)
          }
        >
          <img
            src="/chairSeat.png"
            alt="seat"
            className={`${styles.image} ${styles.seat} postion-absolute h-100 w-auto`}
          />
          <span className={`${styles.seatNumber} postion-absolute`}>{j * ROWS_COUNT + i}</span>
        </div>,
      );
    }
    if (remSeats > 0) {
      curRow.push(
        <div
          className={`${styles.seatWrapper} ${props.reservedSeats[perRowSeats * ROWS_COUNT + i] ? styles.reserved : ""}
          ${!props.isAdmin && props.reservedSeats[perRowSeats * ROWS_COUNT + i] ? styles.cursorNotAllowed : ""}
          ${props.selectedSeat === perRowSeats * ROWS_COUNT + i ? styles.selected : ""}
          mx-2`}
          key={perRowSeats * ROWS_COUNT + i}
          onClick={
            !props.isAdmin && props.reservedSeats[perRowSeats * ROWS_COUNT + i]
              ? () => {}
              : (e) => props.setSelectedSeat(perRowSeats * ROWS_COUNT + i)
          }
        >
          <img
            src="/chairSeat.png"
            alt="seat"
            className={`${styles.image} ${styles.seat} postion-absolute h-100 w-auto`}
          />
          <span className={`${styles.seatNumber} postion-absolute`}>{perRowSeats * ROWS_COUNT + i}</span>
        </div>,
      );
      remSeats -= 1;
    }
    rows.push(curRow);
  }
  return (
    <>
      <div className="row justify-content-center">
        <div className="p-3 col-auto">
          <img src="/stearing.png" alt="stearing" className={`${styles.image} ${styles.stearing}`} />
        </div>
        <div className="col-auto">
          <div className={`${styles.seatRow} w-100`}>{rows[0]}</div>
          <div className={`${styles.seatRow} w-100`}>{rows[1]}</div>
          <div className={`${styles.seatRow} w-100`}>{rows[2]}</div>
          <div className={`${styles.seatRow} w-100`}>{rows[3]}</div>
          <ul className={`${styles.legend}`}>
            <li>
              <span className={`${styles.reserved}`}></span> Reserved
            </li>
            <li>
              <span className={`${styles.selected}`}></span> Selected
            </li>
            <li>
              <span className={``}></span> Unreserved
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default Seats;
