import { useEffect, useState } from "react";
import { isNewExpression } from "typescript";
import { editRoute, postRoute } from "../../actions/RouteActions/routeActions";
import { IBus, IRoute } from "../../models/models";
import styles from "./EditRoute.module.css";

interface IEditRoute extends Omit<IRoute, "_id"> {
  _id?: string;
  isNew: boolean;
  buses: IBus[];
  editRoute: (routeData: IRoute, routeId: IRoute["_id"]) => any;
  postRoute: (routeData: IRoute) => any;
}

const weekDaysLabels = ["", "S", "M", "T", "W", "T", "F", "S"];

const EditRoute: React.FunctionComponent<IEditRoute> = (props) => {
  const [from, setFrom] = useState<string>("");
  const [to, setTo] = useState<string>("");
  const [price, setPrice] = useState<string>("");
  const [departureHour, setDepartureHour] = useState<string>("");
  const [departureMinute, setDepartureMinute] = useState<string>("");
  const [durationHour, setDurationHour] = useState<string>("");
  const [durationMinute, setDurationMinute] = useState<string>("");
  const [days, setDays] = useState<boolean[]>(new Array(8).fill(false));
  const [bus, setBus] = useState<string>('');

  useEffect(() => {
    if (!props.isNew) {
      let travelDurationTime = new Date(props.travelDuration);
      const propDurationHour = travelDurationTime.getHours();
      const propDurationMinute = travelDurationTime.getMinutes();
      setFrom(props.from);
      setTo(props.to);
      setDepartureHour("" + props.departureHour);
      setDepartureMinute("" + props.departureMinute);
      setPrice("" + props.price);
      setDurationHour("" + propDurationHour);
      setDurationMinute("" + propDurationMinute);
      let daysCopy = new Array(8).fill(false);
      for (let weekNum of props.days) {
        daysCopy[weekNum] = true;
      }
      setDays(daysCopy);
      setBus(props.bus);
    }
  }, [props.isNew]);

  const onFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    let travelDurationTime = new Date();
    let daysCopy = [];
    for (let i = 1; i < 8; ++i) {
      if (days[i]) {
        daysCopy.push(i);
      }
    }
    const routeData: IRoute = {
      to: to,
      from: from,
      price: parseInt(price),
      departureHour: parseInt(departureHour),
      departureMinute: parseInt(departureMinute),
      travelDuration: parseInt(durationHour) * 3600000 + parseInt(durationMinute) * 60000,
      days: daysCopy,
      bus: bus,
    };

    console.log(routeData);
    if(props.isNew){
        props.postRoute(routeData);
    }
    else{
        props.editRoute(routeData, props._id as string);
    }
    (window as any).$(`#modal${props._id}`).modal("hide");
  };

  return (
    <div className="container">
      <div className="">
        <form onSubmit={onFormSubmit} className="py-3">

          <div className="row g-2 mb-3">
            <div className="form-floating col">
              <input
                required
                type="text"
                className="form-control"
                id="from"
                value={from}
                onChange={(e) => setFrom(e.target.value)}
                placeholder="From"
              />
              <label htmlFor="from">From</label>
            </div>
            <div className="form-floating col">
              <input
                required
                type="text"
                className="form-control"
                id="To"
                value={to}
                onChange={(e) => setTo(e.target.value)}
                placeholder="Raipur"
              />
              <label htmlFor="To">To</label>
            </div>
          
            <div className="col-auto d-flex align-items-center">
              <div className={`${styles.weekDaysSelector}`}>
                {Array.from({ length: 7 })
                  .map((val, i) => i + 1)
                  .map((weekNum, i) => (
                    <span key={weekNum}
                      className={`${styles.weekDayLabel} ${days[weekNum] ? styles.weekDaySelected : ""}`}
                      onClick={(e) => {
                        let daysCopy = [...days];
                        daysCopy[weekNum] = !days[weekNum];
                        setDays(daysCopy);
                      }}
                    >
                      {weekDaysLabels[weekNum]}
                    </span>
                  ))}
              </div>
            </div>
          </div>

          {/* -------------------------------------------------------------------------------------- */}

          <div className="row mb-3">
            <div className="col-sm-12 col-md-6">
              <label htmlFor="departureHour">Departure Time</label>
              <div className="row g-2">
                <div className="form-floating col">
                  <input
                    required
                    type="number"
                    min={0}
                    max={23}
                    step={1}
                    pattern="\d*"
                    className="form-control"
                    id="departureHour"
                    value={departureHour}
                    onChange={(e) => setDepartureHour(e.target.value)}
                    placeholder="Hours"
                  />
                  <label>Hours</label>
                </div>

                <div className="form-floating col">
                  <input
                    required
                    type="number"
                    pattern="\d*"
                    min={0}
                    max={59}
                    step={1}
                    className="form-control"
                    id="departureMinute"
                    value={departureMinute}
                    onChange={(e) => setDepartureMinute(e.target.value)}
                    placeholder="Minutes"
                  />
                  <label>Minutes</label>
                </div>
              </div>
            </div>

            <div className="form-group col-sm-12 col-md-6">
              <label htmlFor="durationHour">Travel Duration</label>
              <div className="row g-2">
                <div className="form-floating col">
                  <input
                    required
                    type="number"
                    pattern="\d*"
                    min={0}
                    step={1}
                    className="form-control"
                    id="durationHour"
                    value={durationHour}
                    onChange={(e) => setDurationHour(e.target.value)}
                    placeholder="Hours"
                  />
                  <label>Hours</label>
                </div>
                <div className="form-floating col">
                  <input
                    required
                    type="number"
                    pattern="\d*"
                    min={0}
                    max={59}
                    step={1}
                    className="form-control"
                    id="durationMinute"
                    value={durationMinute}
                    onChange={(e) => setDurationMinute(e.target.value)}
                    placeholder="Minutes"
                  />
                  <label>Minutes</label>
                </div>
              </div>
              
            </div>
          </div>

          {/* -------------------------------------------------------------------------------------------------- */}
          <div className="row g-2 mb-3">
            <div className="form-floating col">
              <select
                required
                // type="text"
                // pattern="[a-z0-9]{24}"
                className="form-control"
                id="busId"
                value={bus}
                onChange={(e) => setBus(e.target.value)}
                placeholder="XXXXXXXX"
              >
                <option value="">Select Bus</option>
                {props.buses.map((value) => (
                    <option key={value._id as string} value={value._id as string}>
                      {value._id as string}
                    </option>
                  ))}
                </select>
              <label htmlFor="busId">Bus Id</label>

            </div>
            <div className="form-floating col">
              <input
                required
                type="number"
                className="form-control"
                id="price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                placeholder="1XXX"
              />
              <label htmlFor="price">Price</label>
            </div>
          </div>
          
          <div className="form-group text-center mb-3">
            <input type="submit" className="btn btn-primary btn-lg" value={props.isNew ? "Create" : "Update"} />
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditRoute;
