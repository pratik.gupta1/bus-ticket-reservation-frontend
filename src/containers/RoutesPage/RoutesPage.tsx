import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import withNavbarHoc from "../../hoc/NavbarHoc/NavbarHoc";
import { ICombinedState } from "../../store";
import { BoundThunk, EmptyRoute } from "../../utils";
import routeActions from "../../actions/RouteActions/routeActions";
import EditRoute from "../../components/EditRoute/EditRoute";
import RouteCard from "../../components/RouteCard/RouteCard";
import busActions from "../../actions/BusActions/busActions";

interface IRoutesPageProps {}

const mapStateToProps = (state: ICombinedState, ownProps: IRoutesPageProps) => ({
  routes: state.route.routes,
  isLoggedIn: state.common.isLoggedIn,
  buses: state.bus.buses,
});

const mapDispatchToProps = {...routeActions, ...busActions};
type IDispatchProps = { [P in keyof typeof mapDispatchToProps]: BoundThunk<typeof mapDispatchToProps[P]> };

type IProps = IRoutesPageProps & ReturnType<typeof mapStateToProps> & IDispatchProps & RouteComponentProps;

class RoutesPage extends React.Component<IProps> {
  componentDidMount() {
    if (this.props.isLoggedIn) {
      if (this.props.routes === undefined || this.props.routes.length == 0) {
        this.props.getRoutes();
      }
      if (this.props.buses === undefined || this.props.buses.length == 0) {
        this.props.getBuses();
      }
    } else {
      this.props.history.push("/signin");
      // TODO : show toast for signin required
    }
  }

  render() {
    return (
      <>
        <EditRoute isNew={true} {...EmptyRoute} postRoute={this.props.postRoute} editRoute={this.props.editRoute} buses={this.props.buses || []}/>
        {this.props.routes && this.props.routes.length > 0
          ? this.props.routes.map((route) => <RouteCard key={route._id as string} buses={this.props.buses || []} {...route} postRoute={this.props.postRoute} editRoute={this.props.editRoute} deleteRoute={this.props.deleteRoute}/>)
          : null}
      </>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withNavbarHoc(RoutesPage));
