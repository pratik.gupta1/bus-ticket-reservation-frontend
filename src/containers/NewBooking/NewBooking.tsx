import React, { Component } from "react";
import RouteWrapperCard from "../../components/RouteWrapperCard/RouteWrapperCard";
import SearchPanel from "../../components/SearchPanel/SearchPanel";
import { IPersonDetails, IReservation, IRoute, IRouteWrapper } from "../../models/models";
import styles from "./NewBooking.module.css";
import axios from "../../axiosInstance";
import withNavbarHoc from "../../hoc/NavbarHoc/NavbarHoc";

// const tempBus = {
//   images: [],
//   busType: "Axle semi",
//   vehicleNumber: "adsfi34",
//   vehicleMake: "TATA",
//   vehicleModel: "Bus104",
//   numOfSeats: 20,
// };

interface IState {
  routeWrappers: IRouteWrapper[];
}

class NewBooking extends Component<{}, IState> {
  fetchRoutes = async (from: string, to: string, travelDate: string) => {
    const { data } = await axios.get<IRouteWrapper[]>("/route/findRoutes", { params: { from, to, travelDate } });
    this.setState((prevState) => ({ routeWrappers: data }));
  };

  state : IState = {
    routeWrappers: [],
  };

  addReservation = async (routeId: string, reservation: IReservation)=>{
    const routeWrappers = [...this.state.routeWrappers]
    const ind = routeWrappers.findIndex(e=> e._id===routeId);
    routeWrappers[ind] = {...routeWrappers[ind]}
    routeWrappers[ind].reservations.push(reservation);
    this.setState((prevState)=>({routeWrappers: routeWrappers}));
  }

  render(): JSX.Element {
    return (
      <div className={`w-100`}>
        <div className={`d-flex justify-content-center p-3`}>
          <SearchPanel fetchRoutes={this.fetchRoutes} />
        </div>
        <div>
          { this.state.routeWrappers.length>0 ?
          this.state.routeWrappers.map((wrapper) => (
            <RouteWrapperCard key={wrapper._id} {...wrapper} addReservation={this.addReservation} isAdmin={false}/>
          ))
        :
        <img src="/eldoret-express.gif" className={`${styles.busImage}`}/>
        }
        </div>
      </div>
    );
  }
}

export default withNavbarHoc(NewBooking);
