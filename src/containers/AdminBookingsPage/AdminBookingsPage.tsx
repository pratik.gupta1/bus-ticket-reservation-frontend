import React from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import axios from "../../axiosInstance";
import RouteWrapperCard from "../../components/RouteWrapperCard/RouteWrapperCard";
import SearchPanel from "../../components/SearchPanel/SearchPanel";
import withNavbarHoc from "../../hoc/NavbarHoc/NavbarHoc";
import { IRouteWrapper } from "../../models/models";
import { ICombinedState } from "../../store";

interface IAdminBookingsPageState {
  routeWrappers: IRouteWrapper[];
}

const mapStateToProps = (state: ICombinedState) => ({
  isLoggedIn: state.common.isLoggedIn,
});

type IProps = ReturnType<typeof mapStateToProps> & RouteComponentProps;

class AdminBookingsPage extends React.Component<IProps, IAdminBookingsPageState> {
  componentDidMount() {
    if (this.props.isLoggedIn) {
    } else {
      this.props.history.push("/signin");
      // TODO : show toast for signin required
    }
  }

  state: IAdminBookingsPageState = {
    routeWrappers: [],
  };

  fetchRoutes = async (from: string, to: string, travelDate: string) => {
    const { data } = await axios.get<IRouteWrapper[]>("/route/findRoutesWithDetails", {
      params: { from, to, travelDate },
    });
    this.setState((prevState) => ({ routeWrappers: data }));
  };

  cancelReservation = async (routeId: string, reservationId: string) => {
    const routeWrappers = [...this.state.routeWrappers];
    const ind = routeWrappers.findIndex((e) => e._id === routeId);
    routeWrappers[ind] = { ...routeWrappers[ind] };
    const resInd = routeWrappers[ind].reservations.findIndex((e) => e._id === reservationId);
    routeWrappers[ind].reservations.splice(resInd, 1);
    routeWrappers[ind].reservations = [...routeWrappers[ind].reservations];
    this.setState((prevState) => ({ routeWrappers: routeWrappers }));
  };

  render() {
    return (
      <div className={`w-100`}>
        <div className={`d-flex justify-content-center`}>
          <SearchPanel fetchRoutes={this.fetchRoutes} />
        </div>
        <div>
          {this.state.routeWrappers.map((wrapper) => (
            <RouteWrapperCard
              key={wrapper._id}
              {...wrapper}
              isAdmin={true}
              cancelReservation={this.cancelReservation}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, null)(withNavbarHoc(AdminBookingsPage));
