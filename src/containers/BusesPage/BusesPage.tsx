import React from "react";
import withNavbarHoc from "../../hoc/NavbarHoc/NavbarHoc";
import { ICombinedState } from "../../store";
import busActions from "../../actions/BusActions/busActions";
import { connect } from "react-redux";
import BusCard from "../../components/BusCard/BusCard";
import { RouteComponentProps } from "react-router-dom";
import { BoundThunk, EmptyBus, isSignedIn } from "../../utils";
import EditBus from "../../components/EditBus/EditBus";

interface IBusesPageProps {}

const mapStateToProps = (state: ICombinedState, ownProps: IBusesPageProps) => ({
  buses: state.bus.buses,
  isLoggedIn: state.common.isLoggedIn,
});
// const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, ActionTypes>, ownProps: IBusesProps)=> bindActionCreators(busActions, dispatch)

const mapDispatchToProps = busActions;
type IDispatchProps = { [P in keyof typeof mapDispatchToProps]: BoundThunk<typeof mapDispatchToProps[P]> };
type IProps = IBusesPageProps & IDispatchProps & ReturnType<typeof mapStateToProps> & RouteComponentProps;

class BusesPage extends React.Component<IProps> {
  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.getBuses();
    } else {
      this.props.history.push("/signin");
      // TODO : show toast for signin required
    }
    // isSignedIn().then((value) => {
    //   if (value) {
    //     this.props.getBuses();
    //   } else {
    //     this.props.history.push("/signin");
    //     // TODO : show toast for signin required
    //   }
    // });
  }

  render() {
    return (
      <>
        <div className="container card my-4">
          <div className="d-flex justify-content-between p-3">
            <span className="fs-5">Add bus</span>
            <i
              className={`large material-icons`}
              data-bs-toggle="collapse"
              data-bs-target="#addBus"
              role="button"
              aria-expanded="false"
              aria-controls="addBus"
            >
              expand_more
            </i>
          </div>
          <div id="addBus" className="collapse">
            <EditBus {...EmptyBus} isNew={true} postBus={this.props.postBus} editBus={this.props.editBus} />
          </div>
        </div>
        {this.props.buses &&
          this.props.buses.map((bus) => (
            <BusCard
              key={bus._id as string}
              {...bus}
              editBus={this.props.editBus}
              deleteBus={this.props.deleteBus}
              postBus={this.props.postBus}
            />
          ))}
      </>
    );
  }
}

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector(withNavbarHoc(BusesPage));
