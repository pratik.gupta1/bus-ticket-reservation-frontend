export const LOADING = "LOADING";
export const AUTH = "AUTH";

export interface ILoadingAction {
    type: typeof LOADING;
    isLoading: boolean;
}

export interface IAuthAction{
    type: typeof AUTH;
    isLoggedIn: boolean;
}

type CommonActionTypes = ILoadingAction | IAuthAction;
export default CommonActionTypes;