import axios from "../../axiosInstance";
import { Dispatch } from "redux";
import { ICombinedState } from "../../store";
import ActionTypes, { LOADING } from "../actionTypes";
import { IRoute } from "../../models/models";
import { DELETE_ROUTE, EDIT_ROUTE, GET_ROUTES, POST_ROUTE } from "./routeActionTypes";
import { showError } from "../../utils";

export const getRoutes = () => async (dispatch: Dispatch<ActionTypes>, getState: () => ICombinedState) => {
  dispatch({
    type: LOADING,
    isLoading: true,
  });

  try {
    const { data, status } = await axios.get<IRoute[]>("/route");
    dispatch({ type: GET_ROUTES, routes: data });
  } catch (error) {
    showError("Error fetching routes", dispatch);
  }

  dispatch({ type: LOADING, isLoading: false });
};

export const postRoute = (routeData: IRoute) => async (
  dispatch: Dispatch<ActionTypes>,
  getState: () => ICombinedState,
) => {
  try {
    console.log("post route accessed");
    const { data } = await axios.post<IRoute>("/route", routeData);
    dispatch({ type: POST_ROUTE, routeData: data });
  } catch (error) {
    showError("Error posting route", dispatch);
  }
};

export const editRoute = (routeData: IRoute, routeId: IRoute["_id"]) => async (
  dispatch: Dispatch<ActionTypes>,
  getState: () => ICombinedState,
) => {
  try {
    const { data } = await axios.put<IRoute>(`/route/${routeId}`, routeData);
    dispatch({ type: EDIT_ROUTE, routeData: data, routeId: routeId });
  } catch (error) {
    showError("Error editing route", dispatch);
  }
};

export const deleteRoute = (routeId: IRoute["_id"]) => async (
  dispatch: Dispatch<ActionTypes>,
  getState: () => ICombinedState,
) => {
  try {
    const { data } = await axios.delete<any>(`/route/${routeId}`);
    dispatch({ type: DELETE_ROUTE, routeId: routeId });
  } catch (error) {
    showError("Error deleting route", dispatch);
  }
};

export default {
    getRoutes,
    postRoute,
    editRoute,
    deleteRoute,
}
