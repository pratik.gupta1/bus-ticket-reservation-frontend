import { IBus } from "../../models/models";

export const GET_BUSES = "GET_BUSES";
export const POST_BUS = "POST_BUS";
export const DELETE_BUS = "DELETE_BUS";
export const EDIT_BUS = "EDIT_BUS";

export interface IGetBusesAction {
    type: typeof GET_BUSES;
    buses: IBus[];
}
export interface IPostBusAction {
    type: typeof POST_BUS;
    busData: IBus;
}
export interface IDeleteBusAction {
    type: typeof DELETE_BUS;
    busId: IBus["_id"];
}
export interface IEditBusAction {
    type: typeof EDIT_BUS;
    busData: IBus;
    busId: IBus["_id"];
}

type BusActionTypes =  IGetBusesAction | IPostBusAction | IDeleteBusAction | IEditBusAction;
export default BusActionTypes;